#include<iostream>
#include<vector>
#include<ctime>
#include<cstdlib>
#include<iomanip>
using namespace std;

void inVector(vector<int> &number);
void sum(vector<int> &number);
void highest(vector<int> &number);
void lowest(vector<int> &number);
void mean(vector<int> &number);
void median(vector<int> &number);
void mode(vector<int> &number);
void even(vector<int> &number);
void odd(vector<int> &number);
void sort(vector<int> &number);

int main(){
	vector<int> number;				//variable of vector 
	
	inVector(number);
	sum(number);
	highest(number);
	lowest(number);
	mean(number);
	median(number);
	mode(number);
	even(number);
	odd(number);
	sort(number);
	
	return 0;
}

void inVector(vector<int> &number){				//function random number and put into vector
		srand(time(0));
		int num; 
	for(int i=0; i<rand()%101+50; i++){			//O(n)
		num = rand()%101;
		number.push_back(num);					//put number into vector
		}	
/*	for(int j=0; j<number.size(); j++){			//original vector before sort
		cout << setw(5) << number[j] ;
			if(j%10==9){
			cout << endl;
		}	
	} 
	cout << endl; */
	cout << "The number of elements : " << number.size(); 
}

void sum(vector<int> &number){					//function sum
	int sum;
	for(int i=0; i<number.size(); i++){			//O(n)
		sum = sum + number[i];
	}
	cout << "\nThe sum of all elements :" << sum;
}

void highest(vector<int> &number){					//function find the highest number in vector
	int highest = number[0];
	for(int i=0; i<number.size(); i++){				//O(n)
		if(number[i]>highest){						
			highest = number[i];					//if number(1) more than last the highest number, the highest number equal to number(1)
		}
	}
	cout << "\nThe highest value : " << highest; 
}													

void lowest(vector<int> &number){					//function find the lowest number in vector
	int lowest=number[0];
	for(int i=0; i<number.size(); i++){				//O(n)
		if(number[i]<lowest){
			lowest = number[i];						//if number(1) less than last the lowest number, the lowest number equal to number(1)
		}
	}
	cout << "\nThe lowest value : " << lowest; 
}

void mean(vector<int> &number){						//function calculate mean
	double sum;
	double mean;
	for(int i=0; i<number.size(); i++){				//O(n)
		sum = sum + number[i];
	}
	mean = sum/number.size();
	cout << "\nThe mean value : " << mean;
}

void median(vector<int> &number){					//function calculate median
	int swap, hold;
	double med;
	do{												//in do function, sort from lowest value to hightest value
		swap=1;
		for(int i=0; i<number.size()-1; i++){		//O(n)
			if(number[i]>number[i+1]){
				hold = number[i];
				number[i] = number[i+1];
				number[i+1] = hold;
				swap = 0;
			}
		}
	}while(swap==0);

	if(number.size()%2==1){
	cout <<"\nThe median value : "<< number[number.size()/2];
	}else if(number.size()%2==0){
		med = number[number.size()/2] + number[(number.size()/2)-1];
 	cout << "\nThe median value : " << (med/2)+0.5;	
	}
}

void mode(vector<int> &number){					//function calculate mode
	int max, mode;
	int count[100] = {};
	
	for(int i=0; i<number.size(); i++){			//if function find the same number counter plus 1	//O(n)
		count[number[i]]++;					
	}
	
	max = count[0];
	for(int i=1; i<=100; i++){					//find the highest value of counter 	//O(n)
		if(count[i] > max){
			max = count[i];
			mode = i;
		}
	}
	cout << "\nThe mode vale : "  << mode;
}												//O(2n)

void even(vector<int> &number){					//function find even number and count it 
	int countEven=0;
	for(int i=0; i<number.size(); i++){			//O(n)
		if(number[i]%2==0){
			countEven++;						
		}
	}
	cout << "\nThe number of even number : " << countEven;
}

void odd(vector<int> &number){					//function find odd number and count it
	int countOdd=0;
	for(int i=0; i<number.size(); i++){			//O(n)
		if(number[i]%2==1){
			countOdd++;
		}
	}
	cout << "\nThe number of odd number : " << countOdd;
}

void sort(vector<int> &number){						//function sort the value of vector from the lowest to the highest vale
	int swap, hold;
	do{
		swap=1;
		for(int i=0; i<number.size()-1; i++){		//O(n)
			if(number[i]>number[i+1]){
				hold = number[i];
				number[i] = number[i+1];
				number[i+1] = hold;
				swap = 0;
			}
		}
	}while(swap==0);
		cout << endl << endl;
		cout << "The values output in order from lowest to highest :\n" ;
		for(int j=0; j<number.size(); j++){			//O(n)
		cout << setw(5) << number[j] ;
			if(j%10==9){
			cout << endl;
		}	
	}
}



